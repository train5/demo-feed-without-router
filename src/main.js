import Vue from "vue";
import App from "./App.vue";
import VueAxios from "vue-axios";
import axios from "axios";
import Paginate from "vuejs-paginate";
import {
  BootstrapVue,
  IconsPlugin
} from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
// Install package requets
Vue.use(VueAxios, axios);
// Install pagekage pagination
Vue.component("paginate", Paginate);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");